from playwright.sync_api import Playwright, sync_playwright
from pages.add_shop_survey.add_shop_survey import AddShopSurvey
from pages.main_page.main_page import ShopBaseMainPage
from pages.sign_up.sign_up import SignUpPage



def test_successful_registration(set_up) -> None:     
    page = set_up
    shop_base_main_page = ShopBaseMainPage(page)
    #Click start 14 day free trial button
    shop_base_main_page.click_start_14_day_free_trial_button()
    sign_up_page = SignUpPage(page)
    # Registration with valid details
    sign_up_page.input_field_to_registration(sign_up_page.VALID_EMAIL , sign_up_page.PASSWORD ,sign_up_page.SHOPNAME)
    #Click Sign up button
    sign_up_page.click_sign_up_button()
    #User is successfully registered with the provided details.User is redirected to the dashboard of the newly created ShopBase account.
    add_shop_survey = AddShopSurvey(page)
    add_shop_survey.verify_successfully_registered_redirectto_add_shop_page()


   

