
import playwright
import pytest


@pytest.fixture
def set_up(playwright: playwright) -> None:
    browser = playwright.chromium.launch(headless=False, slow_mo=1000)
    context = browser.new_context()
    page = context.new_page()
    # Open url in  browser    
    page.goto("https://www.shopbase.com/")
    page.set_default_timeout(3000)   
    assert page.title() == "ShopBase | Dropshipping, Print-on-Demand Made Easy"

    yield page

    #Teardown
    context.close()
    page.close()




