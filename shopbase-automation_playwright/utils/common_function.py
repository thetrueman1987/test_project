import subprocess

def get_google_authenticator(secret_key):
    cmd = f"oathtool --totp --base32 {secret_key}"
    output = subprocess.check_output(cmd.split()).decode('utf-8').strip()
    return output