from playwright.sync_api import Page, Locator

class SignUpPage:
    """Clinic Login Page Class"""
    VALID_EMAIL = "thetrueman1987@gmail.com"
    PASSWORD = "Password@123"
    SHOPNAME = "thetrueman1987shop"

    def __init__(self, page: Page):
        self.page = page   

        self.login_title = self.page.locator('//div[@class="unite-ui-login__title"]/h1')        
        #input_email
        self.input_email = self.page.locator('//*[@id="email"]') 
        self.input_email.wait_for()       
        #Input_password
        self.password_field = self.page.locator('//*[@id="password"]')
        self.password_field.wait_for()   
        #shop_name
        self.shopname_field = self.page.locator('//*[@id="shop_name"]')
        self.shopname_field.wait_for() 
        #Signup button 
        self.signup_button = self.page.locator('//button[@type="submit"]')

    def input_field_to_registration(self, mail:str , password:str , shopname:str):
        """Input password""" 
        #Clear and input email
        self.input_email.clear()
        self.input_email.fill(mail) 
        #Clear and input password     
        self.password_field.clear()
        self.password_field.fill(password)
        #Clear and input shopname
        self.shopname_field.clear()
        self.password_field.fill(shopname)

    def click_sign_up_button(self):
        self.signup_button.click()

        
        