from playwright.sync_api import Page, Locator

class AddShopSurvey:
    """Add Shop Survey Page Class"""  
    
    def __init__(self, page: Page):
        self.page = page
        #add_your_contact_label     
        self.add_your_contact_label = self.page.locator('//*[@id="app"]//div[@class="p-font-bold pointer p-bar-desc"]')             
        #input_firstname        
        self.input_firstname = self.page.locator('//*[@id="first-name"]')        
        # input_lastname
        self.input_lastname = self.page.locator('//*[@id="email"]')     
        # input_phone_number
        self.input_phone_number = self.page.locator('//*[@id="phone-number"]')
        # personnal_social_profile
        self.input_personnal_social_profile = self.page.locator('(//input[@class="s-input__inner"])[8]')         
    

    def input_field(self, firstname:str , lastname:str , phonenumber:str , personnal_social_profile:str):
        """Input mail"""        
        self.input_firstname.clear()
        self.input_firstname.fill(firstname)
        self.input_lastname.clear()
        self.input_lastname.fill(lastname)
        self.input_phone_number.clear()
        self.input_phone_number.fill(phonenumber)
        self.input_personnal_social_profile.clear()
        self.input_personnal_social_profile.fill(personnal_social_profile)

    def verify_successfully_registered_redirectto_add_shop_page(self) -> bool:           
               
        return  self.add_your_contact_label.is_visible() and \
                self.input_firstname.is_visible() and \
                self.input_lastname.is_visible() and \
                self.input_phone_number.is_visible() and \
                self.input_personnal_social_profile.is_visible() 
                



