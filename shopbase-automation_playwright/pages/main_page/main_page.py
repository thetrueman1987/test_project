from playwright.sync_api import Page, Locator

class ShopBaseMainPage:
    """Shop Base Main Page Class"""

    VALID_EMAIL = "thetrueman1987@gmail.com"
    INVALID_EMAIL = "invalidmail.com"  
    
    def __init__(self, page: Page):
        self.page = page
        #start_14_day_free_trial_button        
        self.start_14_day_free_trial = self.page.locator('//*[@id="Navbar"]//a[text()="Start 14-Day Free Trial"]')
        self.start_14_day_free_trial.wait_for()             
        #start_free_trial_button        
        self.start_free_trial = self.page.locator('//*[@id="email-form"]/input[@type="submit"]')
        self.start_free_trial.wait_for()
        # Mail
        self.input_email = self.page.locator('//*[@id="email"]')
        self.input_email.wait_for()
        # Features
        self.Features = self.page.locator('//*[@id="Navbar"]//a[(text()="Features")]')
        # Shopbase_Icon
        self.Shopbase_button = self.page.locator('//*[@id="Navbar"]//a[@class="brand w-nav-brand w--current"]//img[1]')         
    

    def input_mail(self, value:str):
        """Input mail"""        
        self.input_email.clear()
        self.input_email.fill(value)

    def click_start_14_day_free_trial_button(self):
        """click start free trial button"""
        self.start_14_day_free_trial.click()



