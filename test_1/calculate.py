def calculate_value(number):
    if number % 2 == 0:
        return number * 2
    elif number % 3 == 0:
        return number * 3
    else:
        return number * 5

# Gọi hàm calculate_value với tham số là số 4
result = calculate_value(4)
print(result) # Kết quả in ra sẽ là 8

# Gọi hàm calculate_value với tham số là số 9
result = calculate_value(9)
print(result) # Kết quả in ra sẽ là 27

# Gọi hàm calculate_value với tham số là số 7
result = calculate_value(7)
print(result) # Kết quả in ra sẽ là 35